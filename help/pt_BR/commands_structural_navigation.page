<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="commands_structural_navigation" xml:lang="pt-BR">
  <info>
    <link type="next" xref="commands_table"/>
    <link type="guide" xref="commands#reading_documents"/>
    <link type="seealso" xref="howto_forms"/>
    <title type="sort">2. Navegação Estrutural</title>
    <title type="link">Navegação estrutural</title>
    <desc>Comandos para navegar por elementos</desc>
    <credit type="author">
      <name>Joanmarie Diggs</name>
      <email>joanied@gnome.org</email>
    </credit>
    <license>
      <p>Atribuição Compartilhada Igual 3.0 — Creative Commons</p>
    </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Conte</mal:name>
      <mal:email>alente.alemao@gmail.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>José Vilmar Estácio De Souza</mal:name>
      <mal:email>vilmar@informal.com.br</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Leônidas Araújo</mal:name>
      <mal:email>leorusvellt@hotmail.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>
  <title>Comandos de navegação estrutural</title>
  <p>Os comandos a seguir podem ser usados para navegar por cabeçalhos, links e outros elementos encontrados em aplicativos paa os quais o <app>Orca</app> fornece suporte para a navegação estrutural.</p>
  <list>
    <item>
      <p>Habilita/desabilita teclas de Navegação Estrutural: <keyseq><key>Tecla Modificadora do Orca</key><key>Z</key></keyseq></p>
    </item>
  </list>
  <section id="headings">
    <title>Cabeçalhos</title>
      <list>
        <item>
          <p>Próximo cabeçalho e cabeçalho anterior: <keyseq><key>H</key></keyseq> e <keyseq><key>Shift</key><key>H</key></keyseq></p>
        </item> 
        <item>
          <p>Exibe uma lista de títulos: <keyseq><key>Alt</key><key>Shift</key><key>H</key></keyseq></p>
        </item>
        <item>
          <p>Próximo cabeçalho e cabeçalho anterior do nível 1: <keyseq><key>1</key></keyseq> e <keyseq><key>Shift</key><key>1</key></keyseq></p>
        </item>
        <item>
          <p>Exibe uma lista de títulos no nível 1: <keyseq><key>Alt</key><key>Shift</key><key>1</key></keyseq></p>
        </item>
        <item>
          <p>Próximo cabeçalho e cabeçalho anterior do nível 2: <keyseq><key>2</key></keyseq> e <keyseq><key>Shift</key><key>2</key></keyseq></p>
        </item> 
        <item>
          <p>Exibe uma lista de títulos no nível 2: <keyseq><key>Alt</key><key>Shift</key><key>2</key></keyseq></p>
        </item>
        <item>
          <p>Próximo cabeçalho e cabeçalho anterior do nível 3: <keyseq><key>3</key></keyseq> e <keyseq><key>Shift</key><key>3</key></keyseq></p>
        </item> 
        <item>
          <p>Exibe uma lista de títulos no nível 3: <keyseq><key>Alt</key><key>Shift</key><key>3</key></keyseq></p>
        </item>
        <item>
          <p>Próximo cabeçalho e cabeçalho anterior do nível 4: <keyseq><key>4</key></keyseq> e <keyseq><key>Shift</key><key>4</key></keyseq></p>
        </item> 
        <item>
          <p>Exibe uma lista de títulos no nível 4: <keyseq><key>Alt</key><key>Shift</key><key>4</key></keyseq></p>
        </item>
        <item>
          <p>Próximo cabeçalho e cabeçalho anterior do nível 5: <keyseq><key>5</key></keyseq> e <keyseq><key>Shift</key><key>5</key></keyseq></p>
        </item> 
        <item>
          <p>Exibe uma lista de títulos no nível 5: <keyseq><key>Alt</key><key>Shift</key><key>5</key></keyseq></p>
        </item>
        <item>
          <p>Próximo cabeçalho e cabeçalho anterior do nível 6: <keyseq><key>6</key></keyseq> e <keyseq><key>Shift</key><key>6</key></keyseq></p>
        </item> 
        <item>
          <p>Exibe uma lista de títulos no nível 6: <keyseq><key>Alt</key><key>Shift</key><key>6</key></keyseq></p>
        </item>
      </list>
    </section>
    <section id="forms">
      <title>Formulários</title>
      <list>
        <item>
          <p>Próximo campo de formulário e campo de formulário anterior: <keyseq><key>Tecla Modificadora do Orca</key><key>Tab</key></keyseq> e <keyseq><key>Orca Modifier</key><key>Shift</key><key>Tab</key></keyseq></p>
        </item> 
        <item>
          <p>Exibe uma lista de campos de formulário: <keyseq><key>Alt</key><key>Shift</key><key>F</key></keyseq></p>
        </item>
        <item>
          <p>Próximo botão e botão anterior: <keyseq><key>B</key></keyseq> e <keyseq><key>Shift</key><key>B</key></keyseq></p>
        </item> 
        <item>
          <p>Exibe uma lista de botões: <keyseq><key>Alt</key><key>Shift</key><key>B</key></keyseq></p>
        </item>
        <item>
          <p>Próxima caixa de combinação e caixa de combinação anterior: <keyseq><key>C</key></keyseq> e <keyseq><key>Shift</key><key>C</key></keyseq></p>
        </item> 
        <item>
          <p>Exibe uma lista de caixas de combinação: <keyseq><key>Alt</key><key>Shift</key><key>C</key></keyseq></p>
        </item>
        <item>
          <p>Próxima entrada e entrada anterior: <keyseq><key>E</key></keyseq> e <keyseq><key>Shift</key><key>E</key></keyseq></p>
        </item> 
        <item>
          <p>Exibe uma lista de entradas: <keyseq><key>Alt</key><key>Shift</key><key>E</key></keyseq></p>
        </item>
        <item>
          <p>Próximo botão de opção e botão de opção anterior: <keyseq><key>R</key></keyseq> e <keyseq><key>Shift</key><key>R</key></keyseq></p>
        </item>
        <item>
          <p>Exibe uma lista de botões de opção: <keyseq><key>Alt</key><key>Shift</key><key>R</key></keyseq></p>
        </item> 
        <item>
          <p>Próxima caixa de seleção e caixa de seleção anterior: <keyseq><key>X</key></keyseq> e <keyseq><key>Shift</key><key>X</key></keyseq></p>
        </item>
        <item>
          <p>Exibe uma lista de caixas de seleção: <keyseq><key>Alt</key><key>Shift</key><key>X</key></keyseq></p>
        </item> 
      </list>
    </section>
    <section id="links">
    <title>Links</title>
      <list>
        <item>
          <p>Link seguinte e anterior: <keyseq><key>K</key></keyseq> e <keyseq><key>Shift</key><key>K</key></keyseq></p>
        </item>
        <item>
          <p>Exibe uma lista de links: <keyseq><key>Alt</key><key>Shift</key><key>K</key></keyseq></p>
        </item>
        <item>
          <p>Próximo link não visitado e link não visitado anterior: <keyseq><key>U</key></keyseq> e <keyseq><key>Shift</key><key>U</key></keyseq></p>
        </item> 
        <item>
          <p>Exibe uma lista de links não visitados: <keyseq><key>Alt</key><key>Shift</key><key>U</key></keyseq></p>
        </item>
        <item>
          <p>Próximo link visitado e link visitado anterior: <keyseq><key>V</key></keyseq> e <keyseq><key>Shift</key><key>V</key></keyseq></p>
        </item> 
        <item>
          <p>Exibe uma lista de links visitados: <keyseq><key>Alt</key><key>Shift</key><key>V</key></keyseq></p>
        </item>
      </list>
    </section>
    <section id="lists">
      <title>Listas</title>
      <list>
        <item>
          <p>Próxima lista e lista anterior: <keyseq><key>L</key></keyseq> e <keyseq><key>Shift</key><key>L</key></keyseq></p>
        </item> 
        <item>
          <p>Exibe uma lista de listas: <keyseq><key>Alt</key><key>Shift</key><key>L</key></keyseq></p>
        </item>
        <item>
          <p>Próximo item da lista e item anterior da lista: <keyseq><key>I</key></keyseq> e <keyseq><key>Shift</key><key>I</key></keyseq></p>
        </item> 
        <item>
          <p>Exibe uma lista de itens da lista: <keyseq><key>Alt</key><key>Shift</key><key>I</key></keyseq></p>
        </item>
      </list>
    </section>
    <section id="tables">
      <title>Tabelas</title>
      <list>
        <item>
          <p>Próxima tabela e tabela anterior: <keyseq><key>T</key></keyseq> e <keyseq><key>Shift</key><key>T</key></keyseq></p>
        </item> 
        <item>
          <p>Exibe uma lista de tabelas: <keyseq><key>Alt</key><key>Shift</key><key>T</key></keyseq></p>
        </item>
        <item>
          <p>Célula à esquerda: <keyseq><key>Alt</key><key>Shift</key><key>Esquerda</key></keyseq></p>
        </item> 
        <item>
          <p>Célula à direita: <keyseq><key>Alt</key><key>Shift</key><key>Direita</key></keyseq></p>
        </item> 
        <item>
          <p>Célula acima: <keyseq><key>Alt</key><key>Shift</key><key>Cima</key></keyseq></p>
        </item> 
        <item>
          <p>Célula abaixo: <keyseq><key>Alt</key><key>Shift</key><key>Baixo</key></keyseq></p>
        </item> 
        <item>
          <p>Primeira célula na tabela: <keyseq><key>Alt</key><key>Shift</key><key>Posição Inicial</key></keyseq></p>
        </item> 
        <item>
          <p>Última célula da tabela: <keyseq><key>Alt</key><key>Shift</key><key>End</key></keyseq></p>
        </item> 
      </list>
    </section>
    <section id="text_blocks">
      <title>Blocos de texto</title>
      <list>
        <item>
          <p>Próximo parágrafo e parágrafo anterior: <keyseq><key>P</key></keyseq> e <keyseq><key>Shift</key><key>P</key></keyseq></p>
        </item> 
        <item>
          <p>Exibe uma lista de parágrafos: <keyseq><key>Alt</key><key>Shift</key><key>P</key></keyseq></p>
        </item>
        <item>
          <p>Próximo citação em bloco e citação em bloco anterior: <keyseq><key>Q</key></keyseq> e <keyseq><key>Shift</key><key>Q</key></keyseq></p>
        </item> 
        <item>
          <p>Exibe uma lista de citações em bloco: <keyseq><key>Alt</key><key>Shift</key><key>Q</key></keyseq></p>
        </item>
        <item>
          <p>Próximo objeto e objeto anterior "objeto largo": <keyseq><key>O</key></keyseq> e <keyseq><key>Shift</key><key>O</key></keyseq></p>
        </item> 
        <item>
          <p>Exibe uma lista de "objetos grandes": <keyseq><key>Alt</key><key>Shift</key><key>O</key></keyseq></p>
        </item>
       </list>
    </section>
    <section id="other">
      <title>Outros elementos</title>
      <list>
        <item>
          <p>Próxima marca e marca anterior: <keyseq><key>M</key></keyseq> e <keyseq><key>Shift</key><key>M</key></keyseq></p>
        </item> 
        <item>
          <p>Exibe uma lista de pontos de referência: <keyseq><key>Alt</key><key>Shift</key><key>M</key></keyseq></p>
        </item>
        <item>
          <p>Próximo separador e separador anterior: <keyseq><key>S</key></keyseq> e <keyseq><key>Shift</key><key>S</key></keyseq></p>
        </item> 
        <item>
          <p>Próximo e anterior "clicável": <keyseq><key>A</key></keyseq> e <keyseq><key>Shift</key><key>A</key></keyseq></p>
        </item> 
        <item>
          <p>Exibe uma lista de "clicáveis": <keyseq><key>Alt</key><key>Shift</key><key>A</key></keyseq></p>
        </item>
        <item>
          <p>Imagem seguinte e anterior: <keyseq><key>G</key></keyseq> e <keyseq><key>Shift</key><key>G</key></keyseq></p>
        </item>
        <item>
          <p>Exibe uma lista de imagens: <keyseq><key>Alt</key><key>Shift</key><key>G</key></keyseq></p>
        </item>
        <item>
          <p>Início e fim do contêiner atual: <keyseq><key>Shift</key><key>Vírgula</key></keyseq> e <keyseq><key>Vírgula</key></keyseq></p>
        </item>
      </list>
    </section>
</page>
