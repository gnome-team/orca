<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="howto_tables" xml:lang="pt-BR">
  <info>
    <link type="guide" xref="index#reading"/>
    <link type="next" xref="howto_forms"/>
    <title type="sort">4. Tabelas</title>
    <desc>Navegando e configurando os cabeçalhos dinâmicos</desc>
    <credit type="author">
      <name>Joanmarie Diggs</name>
      <email>joanied@gnome.org</email>
    </credit>
    <license>
      <p>Atribuição Compartilhada Igual 3.0 — Creative Commons</p>
    </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Conte</mal:name>
      <mal:email>alente.alemao@gmail.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>José Vilmar Estácio De Souza</mal:name>
      <mal:email>vilmar@informal.com.br</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Leônidas Araújo</mal:name>
      <mal:email>leorusvellt@hotmail.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>
  <title>Tabelas</title>
  <p>O <app>Orca</app> fornece vários recursos projetados especificamente para melhorar o acesso a tabelas encontradas em páginas da Web e outros documentos: células configuráveis versus leitura de linha, <link xref="howto_structural_navigation">Navegação Estrutural</link> e Cabeçalhos Dinâmicos.</p>
  <section id="cell_row">
    <title>Célula versus Leitura de Linha</title>
    <p>Considere o processo de examinar a lista de mensagens em sua caixa de entrada. Para que o Orca anuncie o remetente, assunto, data e presença de anexos, você precisaria do <app>Orca</app> para falar a linha. Por outro lado, ao navegar entre as linhas de uma planilha, ouvir a linha inteira pode não ser desejável, devido ao grande número de células em cada linha. Assim, nesse caso, você gostaria que o <app>Orca</app> falasse apenas a célula com foco. Situações semelhantes ocorrem em tabelas de documentos.</p>
    <p>O <app>Orca</app> permite que você personalize se apenas a célula ou a linha completa deve ser lida, para tabelas GUI, tabelas de documentos e planilhas. Como essas configurações são independentes umas das outras, você não precisa escolher um modo de leitura de tabela para se ajustar a vários tipos de tabelas.</p>
    <p>Você pode definir cada uma das preferências de leitura de tabela do <app>Orca</app> aplicativo por aplicativo. Como fazer cada um é descrito no guia nas <link xref="preferences">caixas de diálogo de preferências do <app>Orca</app></link>. As configurações podem ser encontradas na <link xref="preferences_speech">página <gui>Fala</gui></link>.</p>
    <p>Por fim, há também um comando do <app>Orca</app> que permite alternar a leitura de células versus linhas em tempo real para a tabela atualmente ativa: <keyseq><key>Tecla Modificadora do Orca</key><key>F11 </key></keyseq>.</p>
  </section>
  <section id="structural_navigation">
    <title>Navegação estrutural</title>
    <p>Os <link xref="commands_structural_navigation#tables">comandos de navegação estrutural da tabela</link> do <app>Orca</app> permitem localizar tabelas rapidamente, pular imediatamente para a primeira ou última célula de uma tabela e mover para a próxima célula em qualquer direção.</p>
    <p>À medida que você navega entre e dentro de tabelas usando a Navegação Estrutural, o <app>Orca</app> anunciará detalhes adicionais para ajudá-lo a entender sua posição, como as dimensões da tabela que você acabou de inserir e o fato de ter atingido sua borda na direção em que está se movendo.</p>
    <p>Além disso, o <app>Orca</app> fornece <link xref="preferences_table_navigation">opções de apresentação</link> configuráveis que funcionam em conjunto com a Navegação Estrutural e permitem controlar se as coordenadas das células são ou não faladas, se a existência de várias células é anunciada, e se as células de cabeçalhos são anunciadas.</p>
    <note style="tip">
      <title>Não se esqueça de ativar a Navegação Estrutural!</title>
      <p>Dependendo de onde você estiver, pode ser necessário ativar explicitamente a Navegação Estrutural antes de poder usá-la. Para saber mais, leia <link xref="howto_structural_navigation#toggling_required"> quando é necessário ativar a Navegação Estrutural.</link></p>
    </note>
  </section>
  <section id="dynamic_headers">
    <title>Cabeçalhos Dinâmicos</title>
    <p>Muitas das tabelas que você encontrará durante a leitura tem células que servem de cabeçalho para a linha ou coluna. Se o criador dessas tabelas marcou ou não corretamente as células como cabeçalhos é difícil de dizer. Em muitos casos, o texto foi simplesmente formatado para ficar maior e/ou em negrito. E mesmo se a tabela está corretamente marcada, não há garantia que o aplicativo ou kit de ferramentas mostre aquele texto com a informação de cabeçalho às tecnologias assistivas. O suporte aos Cabeçalhos Dinâmicos do <app>Orca</app> possibilita superar estes desafios.</p>
    <steps>
      <title>Definindo os cabeçalhos de coluna</title>
      <item>
        <p>Mova para a linha que contenha os cabeçalhos de coluna.</p>
      </item>
      <item>
        <p>Pressione <keyseq><key>Tecla Modificadora do Orca</key><key>R</key></keyseq> para informar ao <app>Orca</app> que a linha atual é a que possui os cabeçalhos.</p>
      </item>
    </steps>
    <steps>
      <title>Definir cabeçalhos de linha</title>
      <item>
        <p>Mova para a coluna que contenha os cabeçalhos de linha.</p>
      </item>
      <item>
        <p>Pressione <keyseq><key>Tecla Modificadora do Orca</key><key>C</key></keyseq> para informar ao <app>Orca</app> que a coluna atual é a que possui cabeçalhos.</p>
      </item>
    </steps>
    <p>Depois de definir os cabeçalhos das colunas ou das linhas, você deve descobrir que, ao navegar entre as células, o <app>Orca</app> apresentará cada cabeçalho que foi alterado. Ou, em outras palavras, o <app>Orca</app> não apresentará o cabeçalho da coluna repetidamente conforme você se move para cima ou para baixo na coluna atual. Da mesma forma, ele não apresentará o cabeçalho da linha repetidamente à medida que você se move para a esquerda ou para a direita na linha atual. No entanto, se você alterar as linhas e houver cabeçalhos de linha, o cabeçalho associado à nova linha será apresentado. E se você alterar as colunas e houver cabeçalhos de coluna, o cabeçalho associado à nova coluna será apresentado.</p>
    <p>Para limpar os cabeçalhos, simplesmente tecle duas vezes o comando que você usou para defini-los. Assim a <keyseq><key>Tecla Modificadora do Orca</key><key>R</key></keyseq>, clique duas vezes, informa ao <app>Orca</app> que não há cabeçalhos de coluna. <keyseq><key>Tecla Modificadora do Orca</key><key>C</key></keyseq>, clique duas vezes, informa ao <app>Orca</app> que não há cabeçalhos de linha.</p>
  </section>
</page>
