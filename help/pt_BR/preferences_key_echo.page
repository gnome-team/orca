<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="preferences_key_echo" xml:lang="pt-BR">
  <info>
    <title type="link">Eco</title>
    <title type="sort">4. Eco</title>
    <desc>Configurando o que é falado enquanto você digita</desc>
    <link type="guide" xref="preferences#orca"/>
    <link type="next" xref="preferences_key_bindings"/>
    <credit type="author">
      <name>Joanmarie Diggs</name>
      <email>joanied@gnome.org</email>
    </credit>
    <license>
      <p>Atribuição Compartilhada Igual 3.0 — Creative Commons</p>
    </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Conte</mal:name>
      <mal:email>alente.alemao@gmail.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>José Vilmar Estácio De Souza</mal:name>
      <mal:email>vilmar@informal.com.br</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Leônidas Araújo</mal:name>
      <mal:email>leorusvellt@hotmail.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>
  <title>Preferências de Eco</title>
  <section id="keyecho">
    <title>Habilitar eco de teclas</title>
    <p>As configurações de eco de teclas do Orca controlam o que acontece cada vez que você pressiona uma tecla. Para habilitar o eco de teclas, marque a caixa de seleção "Habilitar eco de tecla". Ao fazer isso, caixas de seleção adicionais ficarão disponíveis através das quais você poderá escolher exatamente quais teclas devem ou não haver o eco, para melhor adequar-se às suas necessidades.</p>
    <p>Valor padrão: selecionado</p>  
    <section id="keyecho_enable_alphanumeric">
      <title>Ativar teclas alfabéticas</title>
      <p>Esta opção controla se teclas como <key>a</key>, <key>b</key> e <key>c</key> devem ou não ser faladas quando pressionadas.</p>
      <p>Valor padrão: selecionado</p>
    </section>
    <section id="keyecho_enable_numeric">
      <title>Habilitar teclas numéricas</title>
      <p>Esta opção controla se teclas como <key>1</key>, <key>2</key> e <key>3</key> devem ser faladas quando pressionadas.</p>
      <p>Valor padrão: selecionado</p>
    </section>
    <section id="keyecho_enable_punctuation">
      <title>Habilitar teclas de pontuação</title>
      <p>Esta opção controla se teclas como <key>%</key>, <key>;</key> e <key>?</key> devem ser faladas quando pressionadas.</p>
      <p>Valor padrão: selecionado</p>
    </section>
    <section id="keyecho_enable_space">
      <title>Habilitar barra de espaço</title>
      <p>Esta opção controla se <key>Espaço</key> deve ou não ser falado quando pressionado.</p>
      <p>Valor padrão: selecionado</p>
    </section>
    <section id="keyecho_enable_modifier">
      <title>Habilitar teclas modificadoras</title>
      <p>Esta opção controla se as teclas <key>Shift</key>, <key>Ctrl</key>, <key>Alt</key> e <key>Meta</key> devem ser faladas ou não quando pressionadas.</p>
      <p>Valor padrão: selecionado</p>
    </section>
    <section id="keyecho_enable_function">
      <title>Habilitar teclas de função</title>
      <p>Esta opção controla se as teclas de <key>F1</key> a <key>F12</key> devem ser faladas ou não quando pressionadas.</p>
      <p>Valor padrão: selecionado</p>
    </section>
    <section id="keyecho_enable_action">
      <title>Habilitar teclas de ação</title>
      <p>Esta opção controla se <key>BackSpace</key>, <key>Delete</key>, <key>Enter</key>, <key>Esc</key>, <key>Tab</key> , <key>Page Up</key>, <key>Page Down</key>, <key>Home</key> e <key>End</key> devem ser falados quando pressionados.</p>
      <p>Valor padrão: selecionado</p>
    </section>
    <section id="keyecho_enable_navigation">
      <title>Habilitar teclas de navegação</title>
      <p>Esta opção controla se <key>Esquerda</key>, <key>Direita</key>, <key>Acima</key>, e <key>Baixo</key> devem ser faladas ou não quando pressionadas. Esta opção também se aplica à qualquer combinação de teclas na qual a <key>Tecla Modificadora do Orca</key> está sendo pressionada, por exemplo, quando a revisão plana está sendo utilizada.</p>
      <p>Valor padrão: não selecionado</p>
    </section>
    <section id="keyecho_enable_diacritical">
      <title>Habilitar teclas diacríticas sem espaçamento</title>
      <p>Esta opção controla se "teclas mortas" usadas para gerar letras acentuadas devem ser faladas ou não quando pressionadas.</p>
      <p>Valor padrão: não selecionado</p>
    </section>
  </section>
  <section id="characterecho">
    <title>Habilitar eco por caractere</title>
    <p>Habilitar esta opção faz com que o Orca repita o caractere que você acabou de digitar.</p>
    <p>Embora o eco por caractere pareça bastante semelhante ao eco de tecla das teclas alfabéticas, numéricas e de pontuação, existem diferenças importantes, especialmente com relação às letras acentuadas e outros símbolos para os quais não há tecla dedicada:</p>
    <list>
      <item>
        <p>Eco de teclas faz com que o <app>Orca</app> anuncie o que você <em>pressinou</em>.</p>
      </item>
      <item>
        <p>O eco de caractere faz com que o <app>Orca</app> anuncie o que acabou de ser <em>inserido</em>.</p>
      </item>
    </list>
    <p>Assim, para ter caracteres de acentuação falados enquanto você digita, você deve habilitar o eco de teclas por caractere.</p>
    <p>Valor padrão: não selecionado</p>
    <note style="tip">
      <title>Habilitar o eco de tecla e o eco de caractere</title>
      <p>Se você gosta do eco de teclas e você digita caracteres acentuados frequentemente, considere habilitar ambos. A lógica do eco de caractere do <app>Orca</app> tenta filtrar caracteres falados como resultado do eco de teclas, minimizando assim a probabilidade de "falar em duplicidade" enquanto você digita.</p>
    </note>
  </section>
  <section id="wordandsentenceecho">
    <title>Habilitar eco por palavra e habilitar eco por sentença</title>
    <p>Marcar a caixa de seleção <gui>Habilitar eco por palavra</gui> faz com que o <app>Orca</app> repita a palavra que você acabou de digitar. Da mesma forma, marcar a caixa de seleção <gui>Habilitar eco por sentença</gui> faz com que o <app>Orca</app> ressoe a frase que você acabou de digitar.</p>
    <p>Valor padrão: não selecionado</p>
  </section>
</page>
