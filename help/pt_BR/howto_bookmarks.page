<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="howto_bookmarks" xml:lang="pt-BR">
  <info>
    <link type="guide" xref="index#reviewing"/>
    <title type="sort">6. Marcadores</title>
    <desc>Armazenando e recuperando objetos</desc>
    <credit type="author">
      <name>Joanmarie Diggs</name>
      <email>joanied@gnome.org</email>
    </credit>
    <license>
      <p>Atribuição Compartilhada Igual 3.0 — Creative Commons</p>
    </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Conte</mal:name>
      <mal:email>alente.alemao@gmail.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>José Vilmar Estácio De Souza</mal:name>
      <mal:email>vilmar@informal.com.br</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Leônidas Araújo</mal:name>
      <mal:email>leorusvellt@hotmail.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>
  <title>Marcadores</title>
  <p>O suporte aos marcadores do <app>Orca</app> permite-lhe indicar se um objeto é de seu interesse. Isto inclui os seguintes recursos:</p>
  <list>
    <item>
      <p>Você não está limitado a um único objeto: Você pode definir seis marcadores por ambiente. Também podem ser salvos marcadores que continuarão a existir entre o encerramento de uma seção do Orca e o início de outra.</p>
    </item>
    <item>
      <p>Depois de definir um marcador, você pode navegar até ele mais tarde e fazê-lo independentemente de ter ou não escolhido salvar permanentemente os marcadores associados ao seu ambiente atual. A navegação pode ser baseada no número do marcador, caso você queira pular diretamente para um item específico. Alternativamente, você pode navegar para o próximo marcador ou para o anterior, assim como faz com um objeto de <link xref="howto_structural_navigation">Navegação Estrutural</link>.</p>
    </item>
    <item>
      <p>Os objetos que podem ser marcados podem ser widgets em um aplicativo ou objetos na página da web que você está lendo. O ambiente em que você está determina o que acontece enquanto você navega entre os marcadores: se você estiver em conteúdo da web, o cursor será movido para o marcador para que você possa continuar lendo; caso contrário, a <link xref="howto_flat_review">Revisão Plana</link> será ativada, com o marcador se tornando o item de revisão atual.</p>
    </item>
  </list>
  <p>As associações de teclas específicas relativas a cada uma das tarefas acima podem ser encontradas entre os <link xref="commands_bookmarks">Comandos de marcadores</link>.</p>
</page>
