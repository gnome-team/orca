<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="howto_bookmarks" xml:lang="da">
  <info>
    <link type="guide" xref="index#reviewing"/>
    <title type="sort">6. Bogmærker</title>
    <desc>Gemme og hente objekter</desc>
    <credit type="author">
      <name>Joanmarie Diggs</name>
      <email>joanied@gnome.org</email>
    </credit>
    <license>
      <p>Creative Commons Deling på samme vilkår 3.0</p>
    </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2020-2022&lt;</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ask Hjorth Larsen</mal:name>
      <mal:email>asklarsen@gmail.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>
  <title>Bogmærker</title>
  <p><app>Orca</app>s bogmærker lader dig markere objekter af særlig interesse. De har følgende egenskaber:</p>
  <list>
    <item>
      <p>Du er ikke begrænset til et enkelt objekt: Du kan indstille op til seks bogmærker pr. miljø. Du kan også gemme bogmærker, så de huskes fra Orca-session til Orca-session.</p>
    </item>
    <item>
      <p>Når et bogmærke er indstillet, kan du navigere til det senere. Dette er muligt uanset om du har gemt bogmærkerne tilknyttet dit nuværende miljø. Navigation kan være baseret på bogmærkets nummer, hvis du ønsker at hoppe direkte til et bestemt punkt. Eller du kan navigere til det næste eller forrige bogmærke, ligesom du kan med et <link xref="howto_structural_navigation">Strukturel navigation</link>-objekt.</p>
    </item>
    <item>
      <p>Objekter, som kan bogmærkes, kan være kontroller i et program eller objekter på den webside, du læser. Det nuværende miljø afgør hvad, der sker, efterhånden som du navigerer blandt bogmærkerne: Hvis du er i webindhold, flyttes markøren til bogmærket, så du kan fortsætte med at læse; ellers aktiveres <link xref="howto_flat_review">flad gennemgang</link>, hvorved bogmærket bliver det nuværende punkt til gennemgang.</p>
    </item>
  </list>
  <p>De specifikke tastebindinger som er tilknyttet hver opgave ovenfor kan findes i <link xref="commands_bookmarks">Kommandoer for bogmærker</link>.</p>
</page>
